package com.jemmysolutions.fresh;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Splash extends AppCompatActivity {

    public static ArrayList<FreshItem> cartItems;
    public static int cartNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        cartItems = new ArrayList<>();
        cartNum = 0;
        new TinyDB(this).putInt("cartNum",0);
        Timer timer = new Timer();
        final Handler handler = new Handler();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        SharedPreferences pref = getSharedPreferences("pref",MODE_PRIVATE);
                        finish();
                        if(pref.getBoolean("loggedIn",false))
                            startActivity(new Intent(getApplicationContext(),FoodCategories.class));
                        else
                            startActivity(new Intent(getApplicationContext(),MainScreen.class));
                    }
                });
            }
        }, 2500);
    }
}
