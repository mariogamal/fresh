package com.jemmysolutions.fresh;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.jemmysolutions.fresh.MainScreen.URL;
import static com.jemmysolutions.fresh.Splash.cartNum;

public class TrackOrders extends AppCompatActivity {

    LinearLayout orderParent;
    SharedPreferences preferences;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_orders);

        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ...");
        dialog.setCancelable(false);
        preferences = getSharedPreferences("pref",MODE_PRIVATE);
        orderParent = findViewById(R.id.orderParent);
        trackOrders();

    }

    private void trackOrders(){
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"track_user_orders.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("reqresult",response);
                        displayOrders(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", preferences.getString("user_id",""));
                params.put("confirmation_status", "1");
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void displayOrders (String response){
        try {
            JSONArray array = new JSONArray(response);
            int itemNum = array.length();
            for (int i = 0; i < itemNum; i++)
            {
                JSONObject object = array.getJSONObject(i);

                final String id = object.getString("id");
                final String confirmation_status = object.getString("confirmation_status");
                final String order_items = object.getString("order_items");
                final String delivery_time = object.getString("delivery_time");
                final String created_on = object.getString("created_on");
                final String has_discount = object.getString("has_discount");

                View child = getLayoutInflater().inflate(R.layout.order_detail_layout, null);
                orderParent.addView(child);

                TextView state = child.findViewById(R.id.statu);
                TextView total = child.findViewById(R.id.total);
                TextView time = child.findViewById(R.id.duration);
                TextView date = child.findViewById(R.id.time);
                TextView cancel = child.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if (confirmation_status.equals("waiting"))
                            cancelOrder(id);
                        else
                            getFeedback(id);
                    }
                });

                if (confirmation_status.equals("waiting"))
                {
                    state.setText("قيد الانتظار");
                    time.setText("لم يحدد بعد");
                }
                else
                {
                    cancel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check,0,0,0);
                    cancel.setText("استلمت");
                    cancel.setBackgroundColor(getResources().getColor(R.color.Green));
                    state.setText("قيد التوصيل");
                    time.setText(delivery_time+" دقيقة ");
                }
                date.setText(created_on);


                TextView proName = child.findViewById(R.id.proName);
                TextView qty = child.findViewById(R.id.qty);
                TextView unitPrice = child.findViewById(R.id.unitPrice);

                JSONArray arr = new JSONArray(order_items);
                double totalPrice =0;
                for (int ii=0; ii<arr.length();ii++)
                {
                    JSONObject object2 = arr.getJSONObject(ii);
                    final String name = object2.getString("name");
                    final String QTY = object2.getString("qty");
                    final String Price = object2.getString("price");

                    totalPrice += Double.valueOf(Price)*Integer.valueOf(QTY);

                    if (ii==0)
                    {
                        proName.setText(name);
                        qty.setText(QTY);
                        unitPrice.setText(Price);
                    }
                    else 
                    {
                        proName.setText(proName.getText()+"\n "+name);
                        qty.setText(qty.getText()+"\n "+QTY);
                        unitPrice.setText(unitPrice.getText()+"\n "+Price);
                    }
                }

                if (has_discount.equals("1"))
                    totalPrice = (totalPrice*90)/100;

                total.setText(totalPrice+"");
            }
        }catch (Exception ex){Log.d("reqresult",ex.toString());}
        dialog.dismiss();
    }

    private void getFeedback(final String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View rateView = inflater.inflate(R.layout.rate_order_layout,null);
        builder.setView(rateView);
        final AlertDialog alert = builder.create();
        final RatingBar rateBar = rateView.findViewById(R.id.rateBar);
        final EditText clientReview = rateView.findViewById(R.id.review);
        Button send = rateView.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float rate = rateBar.getRating();
                String review = clientReview.getText().toString();
                confirmOrder(id,review,rate);
                alert.dismiss();
            }
        });
        Button cancel = rateView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    private void confirmOrder(final String id , final String review , final float rate) {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"confirm_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("reqresult",response);
                        Toast.makeText(getApplicationContext(), "تم تأكيد استلام الطلب", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(TrackOrders.this,HistoryOrders.class));
                        finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", preferences.getString("user_id",""));
                params.put("rate", String.valueOf(rate));
                params.put("review", review);
                params.put("order_id", id);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void cancelOrder(final String id) {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"cancle_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("reqresult",response);
                        Toast.makeText(getApplicationContext(), "تم الغاء الطلب", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(TrackOrders.this,HistoryOrders.class));
                        finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", preferences.getString("user_id",""));
                params.put("order_id", id);
                return params;
            }
        };
        queue.add(postRequest);
    }

}
