package com.jemmysolutions.fresh;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    Context ctx;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        ctx = getApplicationContext();
        Log.d("reqresult", "Data: " + remoteMessage.getData().toString());
        String body = remoteMessage.getData().get("body");
        String title = remoteMessage.getData().get("title");
        String data = remoteMessage.getData().get("json_data");
        sendNotification(title,body,data);
    }

    private void sendNotification(String title , String body , String data){

        Intent intent = new Intent(ctx, TrackOrders.class);
        if (body.contains("نأسف"))
            intent= new Intent(ctx, HistoryOrders.class);

        if (body.contains("عرض"))
        {
            intent= new Intent(ctx, HomeScreen.class);
            intent.putExtra("cat_id","7");
        }

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder b = new NotificationCompat.Builder(ctx);

        b.setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.freshlogo)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.freshlogo))
                .setContentTitle(title)
                .setContentText(body)
                .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent);

        //Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //b.setSound(alarmSound);

        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NotificationID.getID(), b.build());
    }
}
