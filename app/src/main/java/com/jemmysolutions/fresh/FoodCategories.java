package com.jemmysolutions.fresh;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minibugdev.drawablebadge.BadgePosition;
import com.minibugdev.drawablebadge.DrawableBadge;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

import static com.jemmysolutions.fresh.MainScreen.URL;
import static com.jemmysolutions.fresh.Splash.cartNum;

public class FoodCategories extends AppCompatActivity {

    ImageView cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_categories);
        cart = (ImageView)findViewById(R.id.cart);
        updateCartNum(cartNum);
        loadBanners();
    }
    @Override
    protected void onResume() {
        super.onResume();
        updateCartNum(cartNum);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateCartNum(cartNum);
    }

    public void updateCartNum(int num){
        if (num==0)
            cart.setImageDrawable(null);
        cart.setImageDrawable(new DrawableBadge.Builder(getApplicationContext())
                .drawableResId(R.drawable.ic_cart)
                .badgeColor(R.color.Orange)
                .badgePosition(BadgePosition.TOP_RIGHT)
                .textColor(R.color.White)
                .showBorder(true)
                .badgeSize(R.dimen._20sdp)
                .badgeBorderSize(R.dimen._1sdp)
                .badgeBorderColor(R.color.White)
                .maximumCounter(99)
                .build()
                .get(num));
    }

    public void loadBanners(){

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = MainScreen.URL+"getBanner.php";

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        fillSlider(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );
        queue.add(getRequest);
    }

    public void fillSlider(JSONArray response){
        try {
            BannerSlider bannerSlider = findViewById(R.id.banner_slider1);
            List<Banner> banners=new ArrayList<>();
            int n = response.length();
            for (int i=0;i<n;i++)
            {
                JSONObject obj = response.getJSONObject(i);
                banners.add(new RemoteBanner(obj.getString("imag_url")));
            }
            bannerSlider.setBanners(banners);
        }catch (Exception ex){}

    }
    public void openDetail(View view) {
        Intent intent = new Intent(this,HomeScreen.class);
        intent.putExtra("cat_id",view.getTag().toString());
        startActivity(intent);
    }

    public void openAccount(View view) {
        startActivity(new Intent(this , AccountScreen.class));
    }

    public void callDialog(View view) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_hotline_layout, null);
        TextView call = dialogView.findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01211118454"));
                startActivity(intent);
            }
        });
        dialogBuilder.setView(dialogView);
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void openCart(View view) {
        startActivity(new Intent(this , CartScreen.class));
    }
}
