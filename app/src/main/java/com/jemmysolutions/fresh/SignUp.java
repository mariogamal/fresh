package com.jemmysolutions.fresh;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.jemmysolutions.fresh.MainScreen.URL;

public class SignUp extends AppCompatActivity {

    EditText name , phone , email , pass , address , SPlocation;
    Spinner citySpinner;
    String[] spinnerID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        name = (EditText)findViewById(R.id.name);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        pass = (EditText)findViewById(R.id.pass);
        address = (EditText)findViewById(R.id.address);
        SPlocation = (EditText)findViewById(R.id.SPlocation);
        citySpinner = (Spinner)findViewById(R.id.citySpinner);

        Intent intent = getIntent();

        try {
            if (!intent.getStringExtra("mail").equals("")) {
                email.setText(intent.getStringExtra("mail"));
                phone.setText(intent.getStringExtra("phone"));
                name.setText(intent.getStringExtra("name"));
                pass.setText(intent.getStringExtra("uID"));
                email.setVisibility(View.GONE);
                pass.setVisibility(View.GONE);
            }
        }catch (Exception ex){}

        getCities();
    }

    public void openHome(View view) {
        if (name.getText().toString().equals("")||phone.getText().toString().equals("")
                ||email.getText().toString().equals("")||pass.getText().toString().equals("")
                ||address.getText().toString().equals("")||SPlocation.getText().toString().equals(""))
            Toast.makeText(this, "برجاء ملئ جميع البيانات", Toast.LENGTH_SHORT).show();
        else if (citySpinner.getSelectedItemPosition()==-0)
            Toast.makeText(this, "برجاء اختيار المدينة", Toast.LENGTH_SHORT).show();
        else
            sendLogin();
    }
    void getCities() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = MainScreen.URL+"cities.php";

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Response", response.toString());
                        fillCitySpinner(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        queue.add(getRequest);
    }

    void fillCitySpinner(JSONArray cities) {
        try {
            int n = cities.length();
            String[] spinnerArray = new String[n + 1];
            spinnerID = new String[n + 1];

            spinnerArray[0] = "اختار المدينة";
            spinnerID[0] = "-1";

            for (int i = 1; i < n + 1; i++) {

                JSONObject jb = (JSONObject) cities.getJSONObject(i - 1);
                String cid = jb.getString("id");
                String cname = jb.getString("name");

                spinnerID[i] = cid;
                spinnerArray[i] = cname;
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_layout, spinnerArray) {
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "gefreg.otf");
                    ((TextView) v).setTypeface(externalFont);
                    if (citySpinner.getSelectedItemPosition() == 0)
                        ((TextView) v).setTextColor(getResources().getColor(R.color.spinnerGRAY));
                    else
                        ((TextView) v).setTextColor(getResources().getColor(R.color.Black));
                    return v;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);

                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "gefreg.otf");
                    ((TextView) v).setTypeface(externalFont);
                    ((TextView) v).setTextColor(getResources().getColor(R.color.Black));
                    return v;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            citySpinner.setAdapter(adapter);

        } catch (Exception ex) {}
    }

    public void sendLogin() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"register.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        if(response.contains("true")) {
                            Toast.makeText(SignUp.this, "تم التسجيل بنجاح , يمكنك الدخول الان", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(SignUp.this,MainScreen.class));
                        }
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("username", name.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("email", email.getText().toString());
                params.put("password", pass.getText().toString());
                params.put("address",  address.getText().toString());
                params.put("special_location",  SPlocation.getText().toString());
                params.put("city_id", spinnerID[citySpinner.getSelectedItemPosition()]);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
