package com.jemmysolutions.fresh;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.jemmysolutions.fresh.MainScreen.URL;

public class EditInfo extends AppCompatActivity {


    SharedPreferences pref;
    ProgressDialog dialog;
    EditText name,phone,email,address,SPlocation;
    Spinner citySpinner;
    String[] spinnerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_info);
        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ..");
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        SPlocation = (EditText)findViewById(R.id.SPlocation);
        pref = getSharedPreferences("pref",MODE_PRIVATE);
        citySpinner = (Spinner)findViewById(R.id.citySpinner);
        getCities();

        if (pref.getBoolean("isSocial",false))
        {
            email.setVisibility(View.GONE);
            ((TextView)findViewById(R.id.pass)).setVisibility(View.GONE);
        }
    }

    public void updateInfo(View view) {
        userUpdate();
    }

    public void getInfo() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"user_data.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        displayInfo(response);
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id",pref.getString("user_id",""));
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void displayInfo(String response){
        try {
            JSONArray array = new JSONArray(response);
            JSONObject object = array.getJSONObject(0);
            String name = object.getString("username");
            String phone = object.getString("phone");
            String email = object.getString("email");
            String address = object.getString("address");
            String SPLoc = object.getString("special_location");
            String city_id = object.getString("city_id");
            this.name.setText(name);
            this.phone.setText(phone);
            this.email.setText(email);
            this.address.setText(address);
            this.SPlocation.setText(SPLoc);
            citySpinner.setSelection(getIndex(city_id));
        }catch (Exception ex){ex.toString();}
        dialog.dismiss();
    }

    void getCities() {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = MainScreen.URL+"cities.php";

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Response", response.toString());
                        fillCitySpinner(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        queue.add(getRequest);
    }

    void fillCitySpinner(JSONArray cities) {
        try {
            int n = cities.length();
            String[] spinnerArray = new String[n + 1];
            spinnerID = new String[n + 1];

            spinnerArray[0] = "اختار المدينة";
            spinnerID[0] = "-1";

            for (int i = 1; i < n + 1; i++) {

                JSONObject jb = (JSONObject) cities.getJSONObject(i - 1);
                String cid = jb.getString("id");
                String cname = jb.getString("name");

                spinnerID[i] = cid;
                spinnerArray[i] = cname;
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_layout, spinnerArray) {
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "gefreg.otf");
                    ((TextView) v).setTypeface(externalFont);
                    if (citySpinner.getSelectedItemPosition() == 0)
                        ((TextView) v).setTextColor(getResources().getColor(R.color.spinnerGRAY));
                    else
                        ((TextView) v).setTextColor(getResources().getColor(R.color.Black));
                    return v;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);

                    Typeface externalFont = Typeface.createFromAsset(getAssets(), "gefreg.otf");
                    ((TextView) v).setTypeface(externalFont);
                    ((TextView) v).setTextColor(getResources().getColor(R.color.Black));
                    return v;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            citySpinner.setAdapter(adapter);
            getInfo();
        } catch (Exception ex) {}
    }

    private int getIndex(String myString){

        int index = 0;

        for (int i=0;i<spinnerID.length;i++){
            if (spinnerID[i].equals(myString)){
                index = i;
            }
        }
        return index;
    }

    public void userUpdate() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"update_user.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        if(response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "تم التحديث بنجاح", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("username", name.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("email", email.getText().toString());
                params.put("user_id", pref.getString("user_id",""));
                params.put("address",  address.getText().toString());
                params.put("special_location",  SPlocation.getText().toString());
                params.put("city_id", spinnerID[citySpinner.getSelectedItemPosition()]);
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void showPasswordDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_password_layout, null);
        dialogBuilder.setView(dialogView);

        final EditText old = (EditText) dialogView.findViewById(R.id.oldPW);
        final EditText newPW = (EditText) dialogView.findViewById(R.id.newPW);

        dialogBuilder.setTitle("تغيير كلمة المرور");
        //dialogBuilder.setMessage("اضف الكود قبل الشراء فقط");
        dialogBuilder.setPositiveButton("اتمام", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                changePassword(old.getText().toString(),newPW.getText().toString());
            }
        });
        dialogBuilder.setNegativeButton("الغاء", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void changePassword(final String oldPW , final String newPW){
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"change_password.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        // response
                        if(response.contains("true"))
                        {
                            Toast.makeText(EditInfo.this, "تم التغيير بنجاح", Toast.LENGTH_SHORT).show();
                        }

                        else
                        {
                            Toast.makeText(EditInfo.this, "كلمة المرور خاطئة", Toast.LENGTH_SHORT).show();
                        }
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id",pref.getString("user_id",""));
                params.put("old", oldPW);
                params.put("new", newPW);
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void changePW(View view) {
        showPasswordDialog();
    }
}
