package com.jemmysolutions.fresh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minibugdev.drawablebadge.BadgePosition;
import com.minibugdev.drawablebadge.DrawableBadge;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.jemmysolutions.fresh.MainScreen.URL;
import static com.jemmysolutions.fresh.Splash.cartNum;

public class HomeScreen extends AppCompatActivity {

    ProgressDialog dialog;
    LinearLayout homeParent ,viewParent;
    ImageView cart;
    String cat_id;
    ArrayList<FreshItem> totalItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ...");

        EditText search = findViewById(R.id.searchProduct);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("reqresult",charSequence.toString());
                searchNFilter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Intent intent = getIntent();
        cat_id = intent.getStringExtra("cat_id");
        Log.d("reqresult" , cat_id);
        homeParent = (LinearLayout)findViewById(R.id.homeParent);
        homeParent = (LinearLayout)findViewById(R.id.parentLayout);
        cart = (ImageView)findViewById(R.id.cart);
        updateCartNum(cartNum);
        getProducts();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            updateCartNum(cartNum);
            handleDisabledItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            updateCartNum(cartNum);
            handleDisabledItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCartNum(int num){
        if (num==0)
            cart.setImageDrawable(null);
        cart.setImageDrawable(new DrawableBadge.Builder(getApplicationContext())
                .drawableResId(R.drawable.ic_cart)
                .badgeColor(R.color.Orange)
                .badgePosition(BadgePosition.TOP_RIGHT)
                .textColor(R.color.White)
                .showBorder(true)
                .badgeSize(R.dimen._20sdp)
                .badgeBorderSize(R.dimen._1sdp)
                .badgeBorderColor(R.color.White)
                .maximumCounter(99)
                .build()
                .get(num));
    }

    public void openCart(View view) {
        startActivity(new Intent(this , CartScreen.class));
    }

    public void openAccount(View view) {

        startActivity(new Intent(this , AccountScreen.class));
    }

    public void getProducts() {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"category_products.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        fetchResponse(response);
                        Log.d("reqresult",response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                        Log.d("reqresult",error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("category_id", cat_id);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void fetchResponse(String result){

        try
        {
            totalItems = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            int itemNum = array.length();
            if (itemNum==0)
                Toast.makeText(this, "لا تتوافر منتجات", Toast.LENGTH_SHORT).show();
            for (int i =0; i<itemNum; i++)
            {
                JSONObject object = array.getJSONObject(i);
                final String itemID = object.getString("id");
                final String itemName = object.getString("name");
                final String itemImg = object.getString("imag_url");
                final String itemPrice = object.getString("item_price");
                FreshItem item = new FreshItem(itemID,itemName,itemImg,itemPrice);
                totalItems.add(item);
            }
            populateList(totalItems);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateList(ArrayList<FreshItem> itemList){
        try
        {
            homeParent.removeAllViews();
            for (int i =0; i<itemList.size(); i++)
            {
                final int[] count = {1};
                View child = getLayoutInflater().inflate(R.layout.home_item_layout, null);
                homeParent.addView(child);
                TextView name = (TextView)child.findViewById(R.id.name);
                TextView price = (TextView)child.findViewById(R.id.price);
                ImageView plus = (ImageView)child.findViewById(R.id.plus);
                ImageView minus = (ImageView)child.findViewById(R.id.minus);
                ImageView picture = (ImageView)child.findViewById(R.id.picture);
                final TextView addToCart = (TextView)child.findViewById(R.id.addToCart);
                final TextView number = (TextView)child.findViewById(R.id.number);
                View separator = (View)child.findViewById(R.id.separator);
                if (i == itemList.size()-1)
                    separator.setVisibility(View.GONE);

                final String itemID = itemList.get(i).id;
                final String itemName =itemList.get(i).name;
                final String itemImg = itemList.get(i).image;
                final String itemPrice = itemList.get(i).price;

                Picasso.with(this).load(itemImg).into(picture);

                child.setTag(itemID);
                name.setText(itemName);
                name.setSelected(true);
                price.setText(itemPrice);
                addToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String itemQty = String.valueOf(count[0]);
                        FreshItem item = new FreshItem(itemID,itemName,itemImg,itemPrice,itemQty);
                        Splash.cartItems.add(item);
                        cartNum++;
                        updateCartNum(cartNum);
                        addToCart.setEnabled(false);
                        addToCart.setBackgroundColor(getResources().getColor(R.color.OrangeFade));
                    }
                });
                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        count[0]++;
                        number.setText(count[0]+"");
                    }
                });

                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (count[0]>1)
                            count[0]--;
                        number.setText(count[0]+"");
                    }
                });
            }

            handleDisabledItems();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleDisabledItems(){
        for (int i =0;i<homeParent.getChildCount(); i++)
        {
            View child = homeParent.getChildAt(i);
            TextView addToCart = child.findViewById(R.id.addToCart);
            if (indexOf(child.getTag().toString())==-1)
            {
                addToCart.setEnabled(true);
                addToCart.setBackgroundColor(getResources().getColor(R.color.Orange));
            }
            else
            {
                addToCart.setEnabled(false);
                addToCart.setBackgroundColor(getResources().getColor(R.color.OrangeFade));
            }
        }
    }

    public int indexOf(String id){
        for (int i =0 ; i< Splash.cartItems.size();i++){
            if (Splash.cartItems.get(i).id.equals(id))
                return i;
        }
        return -1;
    }

    private void searchNFilter (String key)
    {
        try
        {
            Log.d("reqresult filter",key);
            ArrayList<FreshItem> filteredItems = new ArrayList<>();
            int found =0;
            for (int i=0; i<totalItems.size();i++)
            {
                if (totalItems.get(i).name.contains(key))
                {
                    filteredItems.add(totalItems.get(i));
                    found++;
                }
            }
            Log.d("reqresult filter",key+" found "+found);
            populateList(filteredItems);
        }
        catch (Exception ex)
        {
            Toast.makeText(this, "حدث خطأ , برجاء اعادة المحاولة", Toast.LENGTH_SHORT).show();
        }
    }

    public void callDialog(View view) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_hotline_layout, null);
        TextView call = dialogView.findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01211118454"));
                startActivity(intent);
            }
        });
        dialogBuilder.setView(dialogView);
//        dialogBuilder.setNegativeButton("انهاء", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//            }
//        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
