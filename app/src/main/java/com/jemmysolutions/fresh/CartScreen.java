package com.jemmysolutions.fresh;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.jemmysolutions.fresh.MainScreen.URL;
import static com.jemmysolutions.fresh.Splash.cartNum;

public class CartScreen extends AppCompatActivity {

    LinearLayout homeParent ;
    SharedPreferences preferences;
    String json_order;
    boolean inputCode;
    String  has_discount;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_screen);
        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ...");
        dialog.setCancelable(false);
        inputCode = false;
        has_discount = "0";
        preferences = getSharedPreferences("pref",MODE_PRIVATE);
        homeParent = (LinearLayout)findViewById(R.id.homeParent);
        try {
            showItems();
        }
        catch (Exception ex){
            Splash.cartItems = new ArrayList<>();
            Splash.cartNum=0;
            showItems();
        }
        calcTotal();
        TextView confirmOrder = findViewById(R.id.confirmOrder);
        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });
    }

    private void calcTotal(){
        TextView total = findViewById(R.id.totalPrice);
        double totalCost = 0;
        for (int i=0;i<Splash.cartItems.size();i++)
        {
            totalCost+= Double.valueOf(Splash.cartItems.get(i).quantity)*Double.valueOf(Splash.cartItems.get(i).price);
        }
        total.setText(" اجمالى المبلغ = "+totalCost+" جنيه ");
    }

    private void calcDiscount(){
        TextView total = findViewById(R.id.totalPrice);
        double totalCost = 0;
        for (int i=0;i<Splash.cartItems.size();i++)
        {
            totalCost+= Double.valueOf(Splash.cartItems.get(i).quantity)*Double.valueOf(Splash.cartItems.get(i).price);
        }
        totalCost = (totalCost*90)/100;
        total.setText(" اجمالى المبلغ = "+totalCost+" جنيه ");
    }

    private void showItems(){
        for (int i =0; i<Splash.cartItems.size(); i++){
            final String id = Splash.cartItems.get(i).id;
            final int index = i;
            final int[] count = {Integer.valueOf(Splash.cartItems.get(i).quantity)};
            final View child = getLayoutInflater().inflate(R.layout.home_item_layout, null);
            homeParent.addView(child);
            ImageView plus = (ImageView)child.findViewById(R.id.plus);
            ImageView minus = (ImageView)child.findViewById(R.id.minus);
            ImageView picture = (ImageView)child.findViewById(R.id.picture);
            final TextView addToCart = (TextView)child.findViewById(R.id.addToCart);
            final TextView number = (TextView)child.findViewById(R.id.number);
            TextView name = (TextView)child.findViewById(R.id.name);
            TextView price = (TextView)child.findViewById(R.id.price);

            number.setText(Splash.cartItems.get(i).quantity);
            name.setText(Splash.cartItems.get(i).name);
            price.setText(Splash.cartItems.get(i).price);

            Picasso.with(this).load(Splash.cartItems.get(i).image).into(picture);

            addToCart.setText("حذف المنتج");
            addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    homeParent.removeView(child);
                    Splash.cartItems.remove(getIndex(id));
                    cartNum--;
                    calcTotal();
                }
            });
            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count[0]++;
                    number.setText(count[0]+"");
                    Splash.cartItems.get(index).quantity = count[0]+"";
                    calcTotal();
                }
            });

            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count[0]>1)
                        count[0]--;
                    number.setText(count[0]+"");
                    Splash.cartItems.get(index).quantity = count[0]+"";
                    calcTotal();
                }
            });
        }
    }

    private void placeOrder(){
        if (!prepareJson())
        {
            Toast.makeText(this, "برجاء اختيار الطلبات اولاً", Toast.LENGTH_SHORT).show();
            return;
        }
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"place_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        if(response.contains("true")) {
                            dialog.dismiss();
                            finish();
                            Splash.cartItems.clear();
                            cartNum = 0;
                            Toast.makeText(CartScreen.this, "تم ارسال الطلب", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(CartScreen.this,TrackOrders.class));
                        }
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("user_id", preferences.getString("user_id",""));
                params.put("confirmation_status", "waiting");
                params.put("order_items", json_order);
                params.put("has_discount", has_discount);
                Log.d("reqresult coupon",has_discount);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private boolean prepareJson(){
       try
       {
           JSONArray array = new JSONArray();
           for (int i=0; i<Splash.cartItems.size(); i++)
           {
               JSONObject object = new JSONObject();
               object.put("name",Splash.cartItems.get(i).name);
               object.put("qty",Splash.cartItems.get(i).quantity);
               object.put("price",Splash.cartItems.get(i).price);
               array.put(object);
           }
           json_order = array.toString();
       }
       catch (Exception ex){}
       if (Splash.cartItems.size()==0)
           return false;
       else
           return true;
    }

    public void showDiscountDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.code_input_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("كود الخصم");
        dialogBuilder.setMessage("اضف الكود قبل الشراء فقط");
        dialogBuilder.setPositiveButton("اتمام", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                 validateDiscount(edt.getText().toString());
            }
        });
        dialogBuilder.setNegativeButton("الغاء", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void addDiscount(View view) {
        showDiscountDialog();
    }

    public void validateDiscount(final String code){
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"validate_coupon.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        // response
                        if(response.contains("true"))
                        {
                            has_discount = "1";
                            calcDiscount();
                            Toast.makeText(CartScreen.this, "تم اضافة الخصم", Toast.LENGTH_SHORT).show();
                        }

                        else
                        {
                            has_discount = "0";
                            Toast.makeText(CartScreen.this, "الكود خطأ", Toast.LENGTH_SHORT).show();
                        }
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.d("reqresult", error.toString());
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("code", code);
                return params;
            }
        };
        queue.add(postRequest);
    }

    private int getIndex(String myString){

        int index = 0;

        for (int i=0;i<Splash.cartItems.size();i++){
            if (Splash.cartItems.get(i).id.equals(myString)){
                index = i;
            }
        }
        return index;
    }
}
