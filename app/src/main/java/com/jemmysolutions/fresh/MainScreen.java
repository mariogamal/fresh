package com.jemmysolutions.fresh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import io.fabric.sdk.android.Fabric;

public class MainScreen extends AppCompatActivity {

    private static final int RC_SIGN_IN = 101;
    private static final String TAG = "Firebase" ;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mFBCallbackManager;
    private TwitterAuthClient mTwitterAuthClient;
    private boolean FBCalled;
    ProgressDialog dialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    EditText email , password;
    public static String URL = "http://104.218.120.144/~zadmin/fresh/api/";
    private String type;
    boolean isSocial;
    String s_uID , s_Name , s_Phone ,s_Mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_screen);
        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ..");
        pref = getSharedPreferences("pref",MODE_PRIVATE);
        editor = pref.edit();
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);

        mAuth = FirebaseAuth.getInstance();
        initGoogle();
        initFacebook();
        initTwitter();
    }

    public void Login(View view) {
        isSocial = false;
        try {
            sendLogin();
        }catch (Exception ex){}
    }

    public void SignUp(View view) {
        startActivity(new Intent(this,SignUp.class));
    }

    public void sendLogin() {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"login.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        // response
                        if(response.contains("true")) {
                            try {
                                JSONObject object = new JSONObject(response);
                                String id = object.getString("user_id");
                                String isAdmin = object.getString("is_admin");
                                editor.putString("user_id", id);
                                editor.putString("is_admin", isAdmin);
                                if (isAdmin.equals("1"))
                                    FirebaseMessaging.getInstance().subscribeToTopic("admin");
                                else
                                    FirebaseMessaging.getInstance().subscribeToTopic("user");
                                editor.putBoolean("loggedIn", true);
                                editor.putBoolean("isSocial",isSocial);
                                editor.apply();
                                startActivity(new Intent(MainScreen.this, FoodCategories.class));
                                finish();
                            }catch (Exception ex){ Log.d("reqresult", ex.toString());}
                        }
                        else
                        {
                            if (isSocial)
                            {
                                Intent intent = new Intent(MainScreen.this,SignUp.class);
                                intent.putExtra("name",s_Name);
                                intent.putExtra("phone",s_Phone);
                                intent.putExtra("mail",s_Mail);
                                intent.putExtra("uID",s_uID);
                                startActivity(intent);
                                Toast.makeText(MainScreen.this, "برجاء استكمال البيانات", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                                Toast.makeText(getApplicationContext(), "البريد او كلمة المرور خطأ", Toast.LENGTH_SHORT).show();
                        }
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", email.getText().toString());
                params.put("password", password.getText().toString());
                params.put("refresh_token", FirebaseInstanceId.getInstance().getToken());
                params.put("os_version", Build.VERSION.SDK_INT + " V "+Build.VERSION.RELEASE);
                params.put("device_model", Build.MANUFACTURER +" "+ Build.MODEL);

                return params;
            }
        };
        queue.add(postRequest);
    }

    private void initGoogle(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id_me))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void initFacebook(){
        mFBCallbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(this);
        String s = FacebookSdk.getApplicationSignature(this);
        Log.d("fbsign",s);
        LoginManager.getInstance().registerCallback(mFBCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleLoginData(null,loginResult.getAccessToken(),null);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }

    private void initTwitter(){
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(
                getString(R.string.CONSUMER_KEY),
                getString(R.string.CONSUMER_SECRET));
        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();
        Twitter.initialize(twitterConfig);
        mTwitterAuthClient= new TwitterAuthClient();
    }

    private void handleLoginData(TwitterSession session , AccessToken token , GoogleSignInAccount acct){
        try {
            AuthCredential credential;
            if (session!=null)
                credential = TwitterAuthProvider.getCredential(session.getAuthToken().token, session.getAuthToken().secret);

            else if(token!=null)
                credential = FacebookAuthProvider.getCredential(token.getToken());

            else
                credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithCredential:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user,type);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                                updateUI(null,type);
                            }
                        }
                    });
        }
        catch (Exception ex)
        {
            Toast.makeText(this, "تعذر الاتصال , برجاء اعادة المحاولة", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUI(FirebaseUser currentUser , String type) {
        if (currentUser !=null)
        {
            s_Phone = currentUser.getPhoneNumber();
            s_Name = currentUser.getDisplayName();
            s_uID = currentUser.getUid();
            s_Mail = currentUser.getEmail();
            if (s_Mail==null)
                s_Mail="";
            if (s_Mail.equals(""))
            {
                String uID = currentUser.getUid();
                switch (type)
                {
                    case "facebook":
                        s_Mail = uID+"@facebook.com";
                        break;

                    case "google":
                        s_Mail = uID+"@google.com";
                        break;

                    case "twitter":
                        s_Mail = uID+"@twitter.com";
                        break;

                    default:
                        s_Mail = uID+"@facebook.com";
                }
            }
            this.email.setText(s_Mail);
            this.password.setText(s_uID);
            sendLogin();
        }
        else
            Toast.makeText(this, "فشل الاتصال برجاء اعادة المحاولة", Toast.LENGTH_SHORT).show();
    }

    public void signIn(View view) {
        isSocial = true;
        if (view.getTag().toString().equals("g"))
        {
            type = "google";
            FBCalled = false;
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
        else if (view.getTag().toString().equals("f")){
            type = "facebook";
            FBCalled = true;
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList( "email",  "public_profile"));
        }

        else if (view.getTag().toString().equals("t"))
        {
            type = "twitter";
            FBCalled = false;
            mTwitterAuthClient.authorize(this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {
                    Log.d(TAG, "twitterLogin:success" + result);
                    handleLoginData(result.data,null,null);
                }

                @Override
                public void failure(TwitterException exception) {
                    Log.w(TAG, "twitterLogin:failure", exception);
                    updateUI(null,"twitter");
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                handleLoginData(null,null,account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                updateUI(null,"google");
                // ...
            }
        }
        else if (FBCalled)
            mFBCallbackManager.onActivityResult(requestCode, resultCode, data);
        else
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

}
