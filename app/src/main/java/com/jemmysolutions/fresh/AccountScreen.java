package com.jemmysolutions.fresh;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import static com.jemmysolutions.fresh.MainScreen.URL;

public class AccountScreen extends AppCompatActivity {


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_screen);

        pref = getSharedPreferences("pref",MODE_PRIVATE);
        editor = pref.edit();
        dialog = new ProgressDialog(this);
        dialog.setTitle("جار التحميل ..");
    }

    public void openEditInfo(View view) {
        startActivity(new Intent(this, EditInfo.class));
    }

    public void sendLogout() {
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"logout.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        // response
                        if(response.contains("true")) {
                            try {
                                if (pref.getString("is_admin","").equals("1"))
                                    FirebaseMessaging.getInstance().unsubscribeFromTopic("admin");
                                else
                                    FirebaseMessaging.getInstance().unsubscribeFromTopic("user");
                                editor.putString("user_id", "");
                                editor.putString("is_admin", "");
                                editor.putBoolean("loggedIn", false);
                                editor.apply();
                                startActivity(new Intent(AccountScreen.this, MainScreen.class));
                                finish();
                                FirebaseAuth.getInstance().signOut();
                            }catch (Exception ex){ Log.d("reqresult", ex.toString());}
                        }
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("reqresult", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("refresh_token", FirebaseInstanceId.getInstance().getToken());
                params.put("user_id",  pref.getString("user_id",""));

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void callHotLine() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_hotline_layout, null);
        TextView call = dialogView.findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01211118454"));
                startActivity(intent);
            }
        });
        dialogBuilder.setView(dialogView);
//        dialogBuilder.setNegativeButton("انهاء", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//            }
//        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void logOut(View view) {
        sendLogout();
    }

    public void contactUs(View view) {
        callHotLine();
    }

    public void trackOrders(View view) {
        startActivity(new Intent(AccountScreen.this, TrackOrders.class));
    }

    public void historyOrders(View view) {
        startActivity(new Intent(AccountScreen.this, HistoryOrders.class));
    }


}
